# USER ON-BOARDING

## SETUP

To be able to build locally you'll need to do the following:

`ionic cordova platform add android@6.4.0`

How to run on either Emulator or Device:

- `ionic cordova run android --emulator`
- `ionic cordova run android --device`