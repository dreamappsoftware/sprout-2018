import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, Content} from 'ionic-angular';

declare var ApiAIPromises: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: ElementRef;

  private apiInitializedPromise: Promise<void>;

  answers = [];
  conversation = new Array<Object>()
  showEmojiPicker = false;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public ngZone: NgZone
  ) {
    this.apiInitializedPromise = platform.ready().then(() => {
      ApiAIPromises.init({
        clientAccessToken: "393c559878764fc8bc225907fc93b21e"
      }).then(() => {
        ApiAIPromises.requestText({
          query: "begin"
        })
        .then(({result: {fulfillment: {speech}}}) => {
          this.ngZone.run(()=> {
            this.answers.push(speech);
            this.conversation.push({from: "bot", text: speech})
          });
        })
      });
    });
  }

  ask(question) {
    this.conversation.push({from: "user", text: question})
    ApiAIPromises.requestText({
      query: question
    })
    .then(({result: {fulfillment: {speech}}}) => {
       this.ngZone.run(()=> {
         this.answers.push(speech);
         this.conversation.push({from: "bot", text: speech})
       });
    })
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.focus();
    } else {
      this.setTextareaScroll();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  private focus() {
    if (this.messageInput && this.messageInput.nativeElement) {
      this.messageInput.nativeElement.focus();
    }
  }

  private setTextareaScroll() {
    const textarea = this.messageInput.nativeElement;
    textarea.scrollTop = textarea.scrollHeight;
  }
}
